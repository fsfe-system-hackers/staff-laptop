# Setup for Staff Laptops

This repository contains all instructions alongside an Ansible playbook to set
up staff laptops.

[![in docs.fsfe.org](https://img.shields.io/badge/in%20docs.fsfe.org-OK-green)](https://docs.fsfe.org/repodocs/staff-laptop/00_README)

## Installation of the OS

Currently, we use Debian Bullseye. Installation media are available in the Berlin
office or can be
[downloaded](https://cdimage.debian.org/images/unofficial/non-free/images-including-firmware/)
and
[flashed](https://www.debian.org/releases/stable/amd64/ch04s03.en.html#usb-copy-easy)
onto a USB drive.

Once the install is completed via the graphical installer (please encrypt your
hard drive), reboot the computer and continue below. Make a physical note of
both the user and the hard drive encryption passphrase that you can then hand to
whoever will receive the computer so they memorise the passwords and
subsequently destroy the physical note.

## Package installation via Ansible

First, install Ansible and Git on the target machine by running:

```shell
sudo apt install -y ansible git
```

Next, clone this repository into your home directory by running:

```shell
git clone https://git.fsfe.org/fsfe-system-hackers/staff-laptop.git
```

Finally, execute the Ansible playbook on the target host by running:

```shell
cd staff-laptop # Navigate to repository
```

and then the following command:

```shell
ansible-playbook -K -v playbook.yml  # When queried for 'BECOME password', enter your user password
```

This installs all packages commonly used by FSFE staff. This operation will take
a while. When it has completed successfully, reboot the computer.

## Employee configuration

Now that all the needed packages are installed, some further manual
configuration is needed.

### E-mails with Thunderbird

Our current email setup is somewhat involved. You **receive** emails on your
`<username>@fsfe.org` address at the email address you specified at
https://my.fsfe.org as your for 'Primary email'. If you are a staffer at the
FSFE, we created an account for you on our IMAP server. The account is
associated with your FSFE account. Next, please open Thunderbird and use
the same credentials to setup your email account. See this [article on setting up Thunderbird with your FSFE account](https://docs.fsfe.org/en/internal/services/email/fsfe). In addition [this
article](https://kb.mailbox.org/en/private/e-mail-article/setup-with-mozilla-thunderbird)
might help you do that.

If FSFE did not provide you with an IMAP account at the FSFE servers
and you want to be able to send mails from your `<username>@fsfe.org`
email address from Thunderbird, you need to add a second identity.

For this follow the steps below:

1. Right click on your account in the sidebar on the left like
   `username@domain.de` and choose **Settings** in the dropdown.
2. Under **Outgoing Server (SMTP)** you will find the menu for **Manage
   Identities**, click on it
3. A new menu will open. Choose **Add**
4. Fill out all the information required. In the field email address put in
   your fsfe-mail (`<username>@fsfe.org`). Also the
   reply-mail should be (`<username>@fsfe.org`). The
   outgoing-mailserver is `mail.fsfe.org` and the port number `587`. Connection
   security is `STARTTLS`. The username and password are the same that you use
   on https://my.fsfe.org.

### Matrix chat with Element

Matrix is an important communication channel. Element (the client we suggest you
use to chat with the rest of us and everybody else who uses matrix) should
already be installed on your machine. Alternatively, you can always use the web
frontend provided at https://chat.fsfe.org. Again, you login with the same
credentials that work on https://my.fsfe.org. Make sure that the homeserver is
set to `matrix.fsfe.org`. Otherwise this will not work.

![Matrix Login Page](./img/matrix.png)

Once you are logged in you can ping me (`@tobiasd:fsfe.org`) or matthias
(`@mk:fsfe.org`) so that one of us can add you to the Staff room or any other
room that is access-restricted.

Next, you should read the [Element User Guide](https://element.io/user-guide).
You can skip the 'Onboarding' chapter as you have already successfully logged
in. Please pay particular attention to the chapter 'Secure backup' and make sure
you have a way to recover your encrypted chats should you lose your computer.
Another thing that might help is setting up Element on another device and
verifying this new device. Please refer to the aforementioned user guide before
reaching out for somebody to help.

### Password management

Your credentials are very important ant you should most certainly not have to
manage all of them in your head.

#### Passbolt (organisation)

[Passbolt](https://passbolt.com/) is a Free Software password storage and
management service. The FSFE's installation is on
[pass.fsfe.org](https://pass.fsfe.org/) and available to people who are
concerned with sensible passwords used by multiple people in various teams. It
is a web-based service, but uses GPG in the background that encrypts all
passwords securely.

The basic idea is that every user creates a new GPG key upon registration, only
used by Passbolt. The secret key will be stored within a browser add-on. Every
password a user has access to will be encrypted with this key.

The deployment code is [here](https://git.fsfe.org/fsfe-system-hackers/passbolt).

##### Initial Setup

If you belong to the group of people who should have access to a selection of
these passwords, you will be invited via email. Then follow these steps
carefully, as the security of the passwords and your access depends on it:

1. Click on the "get started" button in the e-mail you received from passbolt.
2. You will probably be asked to install the Passbolt Extension in your browser.
   It is available for Firefox and Chromium. If you are done, reload the page.
3. You will be asked to verify the server's data. Make sure that you see
   https://pass.fsfe.org as domain and `4E477C5EA50C5CA2DF941805C438739EE8F30B36`
   as server key. Tick the confirmation box if applicable and click "Next"
4. You will now create your own dedicated GPG for Passbolt. You don't need to
   provide more data, just click "Next"
5. Enter a secure passphrase for your new key. **Please store it safely**!
6. In the next step, you have to download the generated key. This is only
   possible now, and nobody can recover it! **Download it and store it
   securely**!
7. As an additional security layer, you can generate a token. Set a colour you
   like, and memorise it as well as its 3-character representation. It will be
   shown next to the login and other password fields. If it's different than
   what you set initially, you will know that the server is not legitimate and
   somebody is interfering. Please contact your technical contact at the FSFE
   immediately.
8. Now you can log in with your passphrase. Next to the password field, you will
   see the security token.

Please note that your key is saved within your browser inside the extension, so
it is bound to this device and browser. If you ever change browsers or want to
set up another device, it is possible to import the key you've saved earlier.

If you're done, inform your happy technical contact at the FSFE, and ask to be
added to the respective groups you should have access to.

##### Usage

To **view** a password:

1.  Visit the service website.
2.  Afterwards, you will see your email and be able to enter the password of
    your key (not you general FSFE password). Please check the colour/text token
    next to the password field.
3.  You will see all passwords you have access to. You can filter via the groups
    on the left, or search for a password.
4.  In your browser, you can also click on the Passbolt extension to search
    within it directly. This may a neat shortcut for you.

To **add** a new password:

1.  Click on the blue "Create" button
2.  Enter a meaningful name, URL, user & password, and a description if
    necessary
3.  Select this password and share it with the group it belongs to. Please set
    it as the owner ("is owner") to make the passwords not depend on individual
    users.

As of now, there are not classical folders but only a flat list of passwords,
separated into groups. A folder feature is in passbolt's pipeline though.

#### KeepassXC (personal)

If you prefer an offline password manager, this playbook installs one called
[KeepassXC](https://keepassxc.org/docs/KeePassXC_GettingStarted.html#_interface_overview)
including its [web
extension](https://keepassxc.org/docs/KeePassXC_GettingStarted.html#_setup_browser_integration)
for Firefox. The two links in the last sentence should provide all the
information you need to setup and manage your offline password store using
KeepassXC.

### Nextcloud

There is some general documentation of our Nextcloud instance at
https://share.fsfe.org in our wiki [over
here](https://wiki.fsfe.org/TechDocs/Nextcloud). The Nextcloud Sync Client
should already be installed on your machine. You can use it to automatically
sync important folders from Nextcloud with your local file system. Please
consult the [sync client's
documentation](https://docs.nextcloud.com/desktop/latest/index.html) should
anything be unclear.

Once you've setup two-factor authentication using TOTP, ping your technical
contact at the FSFE should you need access to certain folders like `Staff` or
`Team`. 

#### Synchronize the Calendar between Nextcloud and Thunderbird

*This assumes, that you have a recent version of Thunderbird (version 102 or later e.g. from Debian 11)*.

1. With two-factor authentication activated in Nextcloud, all apps that want to access your Nextcloud account will need an app specific password.
   So you will have to set one up for the Thunderbird calendar in the [security section of your users settings](https://share.fsfe.org/settings/user/security).
   There you enter an application name (e.g. *Thunderbird*) to identify the app accessing your account and copy the generated password for later usage.
   The password will only be shown once, so be sure that you have copied it, e.g. into your password manager.
2. Next go to the [calendar app](https://share.fsfe.org/apps/calendar) in Nextcloud.
   At the right hand side of your listed calendars you see the *three dots button*.
   In the menu that shows up when clicking that button, you find an entry **Copy private link**.
3. Now open the calendar overview in your Thunderbird.
   On the left side you find a **+** button in the calendar listing.
   Hit it.
   In the upcoming dialog select **On the network** and click the *Next* button.
   On the next page of the dialog you need to enter your FSFE user name, the private link of the calendar and the application password you created for Thunderbird.
   If you want to have the calendar available off-line, you can tick the checkbox **Offline Support** here.
   Finally hit the **Subscribe** button and Thunderbird will try to add the calendar.
4. Repeat step -3- with all calendars you want to synchronize between Thunderbird and the Nextcloud.

#### Synchronize the Contacts between Nextcloud and Thunderbird

To synchronize your contacts from the Nextcloud with your Thunderbird, you have to install the [CardBook addon](https://docs.nextcloud.com/server/19/user_manual/pim/sync_thunderbird.html#alternative-using-the-cardbook-add-on-contacts-only) in your Thunderbird.

Afterwards you can the *remote link* for your address book in the [Contacts app of your Nextcloud](https://share.fsfe.org/apps/contacts/) at the bottom of the left aside controls beneath the *Contacts settings* and the three dots next to "Contacts". Copy the link so you can add it to the CardBook addon as described on the above link. For this you'll also need the special password / token generated for the synchronization of the calendar.

### Backups with Vorta

There is extensive documentation of our backup process [over
here](https://wiki.fsfe.org/TechDocs/OfficeBackUp). The backup client called
'Vorta' should already be installed on your machine. There are also videos
documenting its use on our Nextcloud in `Staff` > `Tech Sessions` > `Session 3 -
Backups`

### gtimelog task file and overtime calculator

The basic task file for the `gtimelog` tool that is used to record working time has been installed.
In addition the [overtime-calc](https://git.fsfe.org/max.mehl/overtime-calc) script by Max Mehl has been installed.
Please ensure that the file `overtime-calc/config.cfg` in your home directory contains your correct weekly working hours.

A background task is configured to check the log for common errors to facilitate administrative work with the log files.
Users get notified about occuring errors with desktop-notifications.

### Remote desktop with rustdesk

To use rustdesk with the FSFE identity server for it, the network settings have to be edited.
See our [rustdesk docs](https://docs.fsfe.org/techdocs/rustdesk.md).
