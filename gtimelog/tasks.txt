INTERNAL
INTERNAL SmallTasks
INTERNAL SelfOrganisation
INTERNAL StaffManagement
INTERNAL Meetings
INTERNAL Technical
INTERNAL Sysadmin
INTERNAL Fundraising
INTERNAL KnowledgeManagement
INTERNAL Finance
INTERNAL GdprRequests

LEGAL
LEGAL LLW
LEGAL Enquiries
LEGAL Network
LEGAL Reuse
LEGAL Ngi
LEGAL Huawei

POLICY
POLICY PMPC
POLICY SaveCodeShare
POLICY OpenStandards
POLICY RadioLockdown
POLICY RouterFreedom
POLICY Swpat
POLICY Elections
POLICY EuFossPolicyMeeting

PA
PA PublicEvents
PA PublicEventsFOSDEM
PA PublicEventsCommunityMeeting
PA PromotionMaterial
PA FreeYourAndroid
PA Ilovefs
PA Newsletter
PA Website

FOSS4SME

MERCHANDISE

OFF Vacation
OFF Sick
OFF PublicHoliday
